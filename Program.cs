﻿using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using System;
using System.Threading;

namespace AlertsPrototype.Sender
{
    class Program
    {
        static void Main(string[] args)
        {
            var connectionString = CloudConfigurationManager.GetSetting("StorageConnectionString");
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(connectionString);
            CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
            CloudQueue queue = queueClient.GetQueueReference("messages");
            queue.CreateIfNotExists();

            var random = new Random();
            var array = new string[7] { "A", "B", "C", "A, B", "A, C", "B, C", "A, B, C" };

            while(true)
            {
                var recipients = array[random.Next(0, 6)];

                var content = string.Format("{{ sender: 'J', recipients: '{1}', body: 'Hello, World @ {0}' }}", DateTime.UtcNow, recipients);
                CloudQueueMessage message = new CloudQueueMessage(content);
                queue.AddMessageAsync(message);
                Console.WriteLine(content);

                var wait = random.Next(1, 10);
                
                Console.WriteLine("Waiting {0} seconds...", wait);
                Thread.Sleep(wait * 1000);
            }
        }
    }
}